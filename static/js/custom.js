window.addEventListener("load", function() {
    const btn = document.getElementsByClassName('fitko-navbar-icon--back')[0];
    const nav = document.getElementsByClassName('navbar__items')[0];

    // checks URL to see if "back to root" button should be visible
    const checkBackBtnVisibility = () => {
        if (window.location.pathname === '/' && !btn.classList.contains('hidden')) {
            btn.classList.add('hidden');
            nav.classList.add('hidden-back-btn');
        } else if (window.location.pathname !== '/' && btn.classList.contains('hidden')) {
            btn.classList.remove('hidden');
            nav.classList.remove('hidden-back-btn');
        }
    };

    this.setInterval(checkBackBtnVisibility, 200);
});
