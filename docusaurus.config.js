/** @type {import('@docusaurus/types').DocusaurusConfig} */

const projectName = 'Dokumentation zur Deutschen Verwaltungscloud (DVC)'

const baseUrl = process.env.DOCUSAURUS_BASE_URL || '/'
const gitBranch = process.env.GIT_BRANCH || 'main'
const gitlabUrl = process.env.GITLAB_SERVER_URL || 'https://gitlab.opencode.de'
const gitlabProjectPath = process.env.GITLAB_PROJECT_PATH

module.exports = {
  title: projectName,
  url: 'https://docs.fitko.de',
  baseUrl,
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'throw',
  favicon: 'favicon.png',
  i18n: {
    defaultLocale: 'de',
    locales: ['de'],
  },
  themeConfig: {
    docs: {
      sidebar: {
        hideable: true,
      },
    },
    prism: {
      additionalLanguages: ['java'],
    },
    colorMode: {
      disableSwitch: true,
    },
    navbar: {
      title: projectName,
      items: [
        {
          type: 'html',
          value: `<a href="/"><img src="${baseUrl}assets/icons/chevron-left.svg" alt="Zurück" /></a>`,
          position: 'left',
          className: 'fitko-navbar-icon--back',
        },
      ],
    },
    footer: {
      style: 'light',
      copyright: `Diese Seite ist Teil des <a href="https://docs.fitko.de/">Föderalen Entwicklungsportals</a>. Verantwortlich für die Inhalte der Seite sind die jeweiligen Autoren. Wenn nicht anders vermerkt, sind die Inhalte dieser Webseite lizenziert unter der <a href="https://creativecommons.org/licenses/by/4.0/deed.de">Creative Commons Namensnennung 4.0 International Public License (CC BY 4.0)</a>. Die technische Infrastruktur wird betrieben durch die FITKO. Es gilt das <a href="https://www.fitko.de/impressum">Impressum der FITKO</a> und die <a href="https://fitko.de/datenschutz">Datenschutzerklärung der FITKO</a> mit der Maßgabe, dass kein Tracking durchgeführt wird und keine Cookies gesetzt werden.`,
    },
  },
  plugins: [
    require.resolve("@cmfcmf/docusaurus-search-local"),
    '@docusaurus-terminology/parser',
  ],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebar.js'),
          editUrl: ({ version, versionDocsDirPath, docPath }) =>
            `${gitlabUrl}/-/ide/project/${gitlabProjectPath}/edit/${gitBranch}/-/${versionDocsDirPath}/${docPath}`,
          routeBasePath: 'docs',
          breadcrumbs: false,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  scripts: [ {
      src: `${baseUrl}js/custom.js`,
      async: false,
    },
  ],
}
